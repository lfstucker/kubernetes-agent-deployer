#!/bin/bash

while getopts a:p: flag
do
    case "${flag}" in
        p) GCP_PROJECT=${OPTARG};;
        a) SERVICE_ACCOUNT_NAME=${OPTARG};;
    esac
done
declare -a RoleArray=(
  "accesscontextmanager.policyAdmin" # Full access to policies, access levels, and access zones
  "cloudkms.admin" # Provides full access to Cloud KMS resources, except encrypt and decrypt operations. https://cloud.google.com/iam/docs/understanding-roles#cloud-kms-roles
  "cloudkms.cryptoKeyEncrypterDecrypter" # Provides ability to use Cloud KMS resources for encrypt and decrypt operations only.
  "compute.admin" # I don't like this; too open for privs. Can we trim?
  "compute.instanceAdmin.v1" # Permissions to create, modify, and delete virtual machine instances. This includes permissions to create, modify, and delete disks, and also to configure Shielded VM settings.
  "compute.networkAdmin" # Permissions to create, modify, and delete networking resources, except for firewall rules and SSL certificates.
  "container.clusterAdmin" # Required to manage cluster
  "iam.serviceAccountAdmin" # Required to create service account to manage cluster
  "iam.serviceAccountUser" # Why do I need admin and user?
  "iam.roleAdmin" # Provides access to all custom roles in the project.
  "iam.serviceAccountTokenCreator" # Impersonate service accounts (create OAuth2 access tokens, sign blobs or JWTs, etc).
  "resourcemanager.projectIamAdmin" # DESCRIPTION: Provides permissions to administer IAM policies on projects.
)
for r in ${RoleArray[@]}; do
  gcloud projects add-iam-policy-binding $GCP_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT_NAME@$GCP_PROJECT.iam.gserviceaccount.com" --role="roles/$r"
done

##################################
# Now let's echo out our policies for this account.
##################################
gcloud projects get-iam-policy  $GCP_PROJECT \
    --flatten="bindings[].members" \
    --format="table(bindings.role)" \
    --filter="bindings.members:$SERVICE_ACCOUNT_NAME"
