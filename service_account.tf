/*
* Create Service Account and grant the required permissions for cluster management.
*     TODO: go back through here & try to trim down permissions being given (to improve security)
*/
resource "google_service_account" "sa" {
    account_id   = "${var.cluster_name}-sa"
    display_name = "${var.cluster_name}-sa"
}

resource "google_project_iam_member" "serviceAccountUser" {
    project = var.project
    role = "roles/iam.serviceAccountUser"
    member = "serviceAccount:${google_service_account.sa.email}"
}

resource "google_project_iam_member" "clusterAdmin" {
    project = var.project
    role = "roles/container.clusterAdmin"
    member = "serviceAccount:${google_service_account.sa.email}"
}

resource "google_project_iam_member" "containerAdmin" {
    project = var.project
    role = "roles/container.admin"
    member = "serviceAccount:${google_service_account.sa.email}"
}

resource "google_project_iam_member" "workloadIdentityUser" {
    project = var.project
    role   = "roles/iam.workloadIdentityUser"
    member = "serviceAccount:${google_service_account.sa.email}"
}

# To allow service_A to impersonate service_B, grant the Service Account Token Creator on B to A.
resource "google_project_iam_member" "serviceAccountTokenCreator" {
    project = var.project
    role   = "roles/iam.serviceAccountTokenCreator"
    member = "serviceAccount:${google_service_account.sa.email}"
}
