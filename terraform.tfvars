# Your Google Cloud settings
project = "lstucker-01cf16d9"
region  = "us-west1"
zone    = "us-west1-a"

# The project holding the GitLab Agent configs. 
# For this project, the value is the ID of this project.
gitlab_project_id_agent_config = 34710605 

# Network information
subnet_cidr   = "10.0.1.0/24"

# GKE Cluster information
cluster_name = "demo-projects"
gke_num_nodes = 2

# Your GraphQL URL
gitlab_graphql_api_url = "https://gitlab.com/api/graphql"